package com.devcamp.dailycampaign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DailyCampaignApplication {

	public static void main(String[] args) {
		SpringApplication.run(DailyCampaignApplication.class, args);
	}

}
